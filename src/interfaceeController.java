/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author DELL
 */
public class interfaceeController implements Initializable {
    
    @FXML
    private Label kelvin;
    @FXML
    private TextField txtCelsius;
    @FXML
    private void acaoBotao1(ActionEvent event) {
        double resultado1, resultado2 , valor;
        valor = Float.parseFloat(txtCelsius.getText());
        resultado1 = valor + 273;
        resultado2 = 1.8 * valor +32 ;
        System.out.println("Voce converteu!");
        kelvin.setText(txtCelsius.getText() + "C° é igual " + resultado1 + " Kelvin  e  " + resultado2 +" Fahrenheit");
    }
    private void acaoBotao2(ActionEvent event) {
        double resultado1, resultado2 , valor;
        valor = Float.parseFloat(txtCelsius.getText());
        resultado1 = valor - 273;
        resultado2 = 1.8 * (valor - 273)  +32 ;
        System.out.println("Voce converteu!");
        kelvin.setText(txtCelsius.getText() + "K é igual " + resultado1 + " Celsius  e  " + resultado2 +" Fahrenheit");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
